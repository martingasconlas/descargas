package com.martin.ra1;

import javax.swing.*;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Martin on 23/11/2016.
 */
public class Descarga extends SwingWorker<Void, Void>{
    private String urlFichero;
    private String rutaFichero;

    public Descarga(String url, String fichero){
        this.urlFichero=url;
        this.rutaFichero=fichero;
    }
    @Override
        protected Void doInBackground() throws Exception {
        //creo una url
        URL url=new URL(urlFichero);
        //creo la conexion
        URLConnection conexion=url.openConnection();
        //el tamaño del fichero
        int tamano=conexion.getContentLength();

        InputStream is=url.openStream();
        FileOutputStream fos=new FileOutputStream(rutaFichero);
        //creo un vector de 2048 bytes -->2kb
        byte[] bytes= new byte[2048];
        int longitud=0;
        int progresoDescarga=0;

        while((longitud=is.read(bytes))!=-1){
            fos.write(bytes,0, longitud);
            progresoDescarga+=longitud;
            setProgress((int)(progresoDescarga*100/tamano));
            firePropertyChange("tamanoDescarga",0,progresoDescarga);
        }

        is.close();
        fos.close();

        setProgress(100);

        return null;
    }
}
