package com.martin.ra1.gui;

import javax.swing.*;

/**
 * Created by Martin on 23/11/2016.
 */
public class Panel {
     JTextField textField1;
     JPanel panelDescarga;
     JLabel lbTitulo;
     JLabel lbURL;
     JProgressBar progressBar1;
     private JButton btIniciar;
     private JButton btParar;
     private JButton btBorrar;
     private JLabel lbMegas;

     public Panel(String url) {
          lbURL.setText(url);
     }
}
